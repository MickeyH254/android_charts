package com.xxmassdeveloper.mpchartexample;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.view.WindowManager;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.xxmassdeveloper.mpchartexample.custom.DayAxisValueFormatter;
import com.xxmassdeveloper.mpchartexample.custom.MyValueFormatter;
import com.xxmassdeveloper.mpchartexample.custom.XYMarkerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;


public class BarChartActivity extends AppCompatActivity {

    public BarChart barChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_barchart);

        barChart = (BarChart) findViewById(R.id.chart1);

        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            MyAsync myAsync = new MyAsync();

            //myAsync.onPreExecute();
            myAsync.execute();
            //myAsync.progressDialog.dismiss();


            setTitle("BarChart Activity");

        } else {
            AlertDialog.Builder builder = new AlertDialog.
                    Builder(BarChartActivity.this);
            builder.setTitle("Alert!");
            builder.setMessage("Please check your network connection");
            builder.setPositiveButton("OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    });
            builder.create().show();
        }



    }


    public class MyXAxisValueFormatter extends ValueFormatter {

        private String[] mvalues;
        public MyXAxisValueFormatter(String[] values){
            this.mvalues = values;
        }


        public String getBarLabel(BarEntry barEntry) {
            return getFormattedValue(barEntry.getX());
        }
    }

    class MyAsync extends AsyncTask<Void, Void, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = ProgressDialog.show(BarChartActivity.this, "Fetching Data", "please wait");

        }

        @Override
        protected String doInBackground(Void... voids) {

            String result = null;

            try {
                URL url = new URL("http://35.196.112.193/mysql/SELECT%20COUNT(emp_no),%20title%20FROM%20titles%20GROUP%20BY%20title%20ORDER%20BY%20COUNT(emp_no)%20DESC");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                result = inputStreamToString(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        public void onPostExecute(String s) {
            super.onPostExecute(s);

            barChart.setDrawBarShadow(false);
            barChart.setDrawValueAboveBar(true);
            barChart.setMaxVisibleValueCount(20000);
            barChart.setPinchZoom(false);
            barChart.setDrawGridBackground(true);

            ArrayList<BarEntry> barEntries = new ArrayList<>();
            ArrayList<String> job_titles = new ArrayList<>();



            try {
                JSONArray jsonArray = new JSONArray(s);
                for (int i = 0; i < jsonArray.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    JSONObject e = jsonArray.getJSONObject(i);


                    barEntries.add(new BarEntry(i, e.getInt("COUNT(emp_no)")));

                    job_titles.add(e.getString("title"));

                    System.out.println("Titles " + job_titles);
                }

                String [] titles = job_titles.toArray(new String[job_titles.size()]);




                BarDataSet barDataSet = new BarDataSet(barEntries, "Employees");
                barDataSet.setColors(ColorTemplate.COLORFUL_COLORS);

                BarData data = new BarData(barDataSet);
                data.setBarWidth(0.95f);

                ValueFormatter xAxisFormatter = new DayAxisValueFormatter(barChart);

                XAxis xAxis = barChart.getXAxis();
                xAxis.setPosition(XAxisPosition.BOTTOM);
                //xAxis.setTypeface(tfLight);
                xAxis.setDrawGridLines(false);
                xAxis.setGranularity(1f); // only intervals of 1 day
                xAxis.setLabelCount(7);
                xAxis.setValueFormatter(new IndexAxisValueFormatter(titles));

                ValueFormatter custom = new MyValueFormatter("");

                YAxis leftAxis = barChart.getAxisLeft();
                //leftAxis.setTypeface(tfLight);
                leftAxis.setLabelCount(8, false);
                leftAxis.setValueFormatter(custom);
                leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
                leftAxis.setSpaceTop(15f);
                leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

                YAxis rightAxis = barChart.getAxisRight();
                rightAxis.setDrawGridLines(false);
                //rightAxis.setTypeface(tfLight);
                rightAxis.setLabelCount(8, false);
                rightAxis.setValueFormatter(custom);
                rightAxis.setSpaceTop(15f);
                rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

                Legend l = barChart.getLegend();
                l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
                l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
                l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
                l.setDrawInside(false);
                l.setForm(Legend.LegendForm.SQUARE);
                l.setFormSize(9f);
                l.setTextSize(11f);
                l.setXEntrySpace(4f);


                XYMarkerView mv = new XYMarkerView(BarChartActivity.this, xAxisFormatter);
                mv.setChartView(barChart); // For bounds control
                barChart.setMarker(mv); // Set the marke

                barChart.setData(data);
                barChart.setFitBars(true); // make the x-axis fit exactly all bars

                barChart.invalidate();



                 // XAxis xAxis = barChart.getXAxis();
//                xAxis.setValueFormatter(new MyXAxisValueFormatter(titles));
              //  xAxis.setPosition(XAxisPosition.BOTH_SIDED);
//                //xAxis.setGranularity(1);
              //  xAxis.setCenterAxisLabels(true);
//                xAxis.setAxisMinimum(1);


            } catch (JSONException e) {
                e.printStackTrace();
            }

            this.progressDialog.dismiss();

        }



    }

    private String inputStreamToString(InputStream is) {
        String rLine = "";
        StringBuilder answer = new StringBuilder();

        InputStreamReader isr = new InputStreamReader(is);

        BufferedReader rd = new BufferedReader(isr);

        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return answer.toString();
    }


}
